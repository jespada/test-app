package main

import (
	"fmt"
	"net/http"

	"github.com/fukata/golang-stats-api-handler"
)

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hi there, Your request is %s!", r.URL.Path[1:])
}

func main() {
	http.HandleFunc("/metrics", stats_api.Handler)
	http.HandleFunc("/", handler)
	http.ListenAndServe(":8080", nil)
}
