#!/bin/bash

SERVICE_NAME="test-app"

TAG=$(git describe --always --tag)

IMAGE_VERSION=$TAG

# build program
GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o $SERVICE_NAME


#build docker image
docker build --build-arg IMAGE_VERSION=$IMAGE_VERSION -t $SERVICE_NAME:$IMAGE_VERSION -t $SERVICE_NAME:latest .

# Publish it
docker tag $SERVICE_NAME:latest jespada/$SERVICE_NAME:latest
docker push jespada/$SERVICE_NAME:latest

#remove bin
rm -f $SERVICE_NAME
