FROM alpine:latest

MAINTAINER Jorge Espada <jorge.espada@lebara.com>

ARG IMAGE_VERSION

LABEL VERSION=${IMAGE_VERSION}

RUN apk add --no-cache ca-certificates && \
    update-ca-certificates

RUN apk add --no-cache --virtual  wget curl tar 

EXPOSE 8080

ADD test-app /

CMD ["/test-app"]